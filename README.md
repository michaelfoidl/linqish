[![pipeline status](https://gitlab.com/michaelfoidl/linqish/badges/master/pipeline.svg)](https://gitlab.com/michaelfoidl/linqish/commits/master)
[![coverage report](https://gitlab.com/michaelfoidl/linqish/badges/master/coverage.svg)](https://gitlab.com/michaelfoidl/linqish/commits/master)

# linqish

linqish provides an API for creating SQL queries similar to .NET's LINQ for Java.
