package at.michaelfoidl.linqish.helper;

public class Comparator {
    public static int compare(Object a, Object b) {
        if (a instanceof Integer && b instanceof Integer) {
            return ((Integer) a).compareTo((Integer) b);
        } else if (a instanceof Double && b instanceof Double) {
            return ((Double) a).compareTo((Double) b);
        } else if (a instanceof Long && b instanceof Long) {
            return ((Long) a).compareTo((Long) b);
        } else if (a instanceof Boolean && b instanceof Boolean) {
            return ((Boolean) a).compareTo((Boolean) b);
        } else if (a instanceof Character && b instanceof Character) {
            return ((Character) a).compareTo((Character) b);
        } else if (a instanceof String && b instanceof String) {
            return ((String) a).compareTo((String) b);
        } else if (a instanceof Byte && b instanceof Byte) {
            return ((Byte) a).compareTo((Byte) b);
        } else if (a instanceof Short && b instanceof Short) {
            return ((Short) a).compareTo((Short) b);
        } else if (a instanceof Float && b instanceof Float) {
            return ((Float) a).compareTo((Float) b);
        } else {
            throw new IllegalStateException("Class " + a.getClass().getCanonicalName() + " is currently not supported for comparison.");
        }
    }
}
