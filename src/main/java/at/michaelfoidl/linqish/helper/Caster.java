package at.michaelfoidl.linqish.helper;

public class Caster {
    public static <TSource, TTarget> TTarget cast(TSource source, Class<TTarget> targetClass) {
        if (targetClass.isAssignableFrom(source.getClass())) {
            //noinspection unchecked
            return (TTarget)source;
        } else {
            throw new IllegalArgumentException(targetClass.getCanonicalName() + " is not assignable from " + source.getClass().getCanonicalName() + ".");
        }
    }
}
