package at.michaelfoidl.linqish.queryable;

import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;

public interface Queryable<TSource> {
    boolean any();

    boolean any(Predicate<TSource> predicate);

    <TTarget> Queryable<TTarget> castTo(Class<TTarget> targetClass);

    long count();

    long count(Predicate<TSource> predicate);

    <TTarget> Comparable<TTarget> min(Function<TSource, Comparable<TTarget>> function);

    TSource minElement(Function<TSource, Comparable> function);

    <TTarget> Comparable<TTarget> max(Function<TSource, Comparable<TTarget>> function);

    TSource maxElement(Function<TSource, Comparable> function);

    <TTarget> Queryable<TTarget> select(Function<TSource, TTarget> function);

    <TTarget> Queryable<TTarget> selectMany(Function<TSource, Queryable<TTarget>> function);

    TSource single();

    TSource singleOrDefault();

    List<TSource> toList();

    Queryable<TSource> where(Predicate<TSource> predicate);
}
