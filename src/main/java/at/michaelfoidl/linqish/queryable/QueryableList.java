package at.michaelfoidl.linqish.queryable;

import at.michaelfoidl.linqish.helper.Caster;
import at.michaelfoidl.linqish.helper.Comparator;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class QueryableList<T> implements Queryable<T> {
    protected ArrayList<T> content;

    public QueryableList() {
        this.content = new ArrayList<>();
    }

    public QueryableList(Collection<T> elements) {
        if (elements != null) {
            this.content = new ArrayList<>(elements);
        }
    }

    @Override
    public boolean any() {
        return this.content.size() > 0;
    }

    @Override
    public boolean any(Predicate<T> predicate) {
        return this.content.stream().anyMatch(predicate);
    }

    @Override
    public <TTarget> Queryable<TTarget> castTo(Class<TTarget> targetClass) {
        return new QueryableList<>(this.content.stream().map(i -> Caster.cast(i, targetClass)).collect(Collectors.toList()));
    }

    @Override
    public long count() {
        return this.content.size();
    }

    @Override
    public long count(Predicate<T> predicate) {
        return this.content.stream().filter(predicate).count();
    }

    @Override
    public <TTarget> Comparable<TTarget> min(Function<T, Comparable<TTarget>> function) {
        Optional<Comparable<TTarget>> result = this.content.stream().map(function).min(Comparator::compare);
        if (result.isPresent()) {
            return result.get();
        } else {
            throw new IllegalStateException("You cannot find the minimum of an empty list.");
        }
    }

    @Override
    public T minElement(Function<T, Comparable> function) {
        Optional<T> result = this.content.stream().min((a, b) -> Comparator.compare(function.apply(a), function.apply(b)));
        return result.orElse(null);
    }

    @Override
    public <TTarget> Comparable<TTarget> max(Function<T, Comparable<TTarget>> function) {
        Optional<Comparable<TTarget>> result = this.content.stream().map(function).max(Comparator::compare);
        if (result.isPresent()) {
            return result.get();
        } else {
            throw new IllegalStateException("You cannot find the maximum of an empty list.");
        }
    }

    @Override
    public T maxElement(Function<T, Comparable> function) {
        Optional<T> result = this.content.stream().max((a, b) -> Comparator.compare(function.apply(a), function.apply(b)));
        return result.orElse(null);
    }

    @Override
    public <TTarget> Queryable<TTarget> select(Function<T, TTarget> function) {
        return new QueryableList<>(this.content.stream().map(function).collect(Collectors.toList()));
    }

    @Override
    public <TTarget> Queryable<TTarget> selectMany(Function<T, Queryable<TTarget>> function) {
        return new QueryableList<>(this.content.stream().map(function).flatMap(i -> i.toList().stream()).collect(Collectors.toList()));
    }

    @Override
    public T single() {
        if (this.content.size() == 1) {
            return this.content.get(0);
        } else {
            throw new IllegalStateException("You cannot pick a single element of a list containing more or less than one element.");
        }
    }

    @Override
    public T singleOrDefault() {
        if (this.content.size() == 1) {
            return this.content.get(0);
        } else {
            if (this.content.size() == 0) {
                return null;
            } else {
                throw new IllegalStateException("You cannot pick a single element of a list containing more than one element.");
            }
        }
    }

    @Override
    public List<T> toList() {
        return this.content;
    }

    @Override
    public Queryable<T> where(Predicate<T> predicate) {
        return new QueryableList<>(this.content.stream().filter(predicate).collect(Collectors.toList()));
    }
}
