package at.michaelfoidl.linqish.tests.queryable.helpers

class SimpleTestObject {
    String aString = "Hello"
    int aInteger = 42
    double aDouble = 3.1415
}
