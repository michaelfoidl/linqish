package at.michaelfoidl.linqish.tests.queryable

import at.michaelfoidl.linqish.queryable.QueryableList
import at.michaelfoidl.linqish.tests.queryable.helpers.ComplexTestObject
import at.michaelfoidl.linqish.tests.queryable.helpers.ListGenerator
import at.michaelfoidl.linqish.tests.queryable.helpers.SimpleTestObject

class QueryableListTests extends QueryableTests<QueryableList<SimpleTestObject>, QueryableList<ComplexTestObject>> {
    @Override
    QueryableList<SimpleTestObject> generateQueryableWithSimpleContent() {
        return ListGenerator.generateSimpleContent()
    }

    @Override
    QueryableList<ComplexTestObject> generateQueryableWithComplexContent() {
        return ListGenerator.generateComplexContent()
    }

    @Override
    QueryableList<SimpleTestObject> generateQueryableWithoutContent() {
        return new QueryableList<SimpleTestObject>()
    }
}