package at.michaelfoidl.linqish.tests.queryable.helpers

import at.michaelfoidl.linqish.queryable.QueryableList

class ListGenerator {
    static QueryableList<SimpleTestObject> generateSimpleContent() {
        return new QueryableList<SimpleTestObject>([
                new SimpleTestObject(aDouble: 2.5, aInteger: 8, aString: "test"),
                new SimpleTestObject(aDouble: 9998.4656, aInteger: 9, aString: "hello"),
                new SimpleTestObject(aDouble: 82.5, aInteger: 10, aString: "abc"),
                new SimpleTestObject(aDouble: 2.1325, aInteger: 7, aString: "xyz"),
                new SimpleTestObject(aDouble: 0.594, aInteger: 8, aString: "linqish"),
                new SimpleTestObject(aDouble: 1.0, aInteger: -45, aString: "a very long string"),
        ])
    }

    static QueryableList<ComplexTestObject> generateComplexContent() {
        return new QueryableList<ComplexTestObject>([
                new ComplexTestObject(aDouble: 2.5, aInteger: 8, aString: "test"),
                new ComplexTestObject(aDouble: 9998.4656, aInteger: 9, aString: "hello"),
                new ComplexTestObject(aDouble: 82.5, aInteger: 10, aString: "abc"),
                new ComplexTestObject(aDouble: 2.1325, aInteger: 7, aString: "xyz"),
                new ComplexTestObject(aDouble: 0.594, aInteger: 8, aString: "linqish"),
                new ComplexTestObject(aDouble: 1.0, aInteger: -45, aString: "a very long string"),
        ])
    }
}
