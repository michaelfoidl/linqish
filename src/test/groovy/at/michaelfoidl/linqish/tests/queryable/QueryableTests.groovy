package at.michaelfoidl.linqish.tests.queryable

import at.michaelfoidl.linqish.queryable.Queryable
import at.michaelfoidl.linqish.tests.queryable.helpers.ComplexTestObject
import at.michaelfoidl.linqish.tests.queryable.helpers.SimpleTestObject
import spock.lang.Ignore
import spock.lang.Specification

import java.util.function.Function
import java.util.function.Predicate

@Ignore
abstract class QueryableTests<TSimple extends Queryable<SimpleTestObject>, TComplex extends Queryable<ComplexTestObject>> extends Specification {

    abstract TSimple generateQueryableWithSimpleContent()

    abstract TSimple generateQueryableWithoutContent()

    abstract TComplex generateQueryableWithComplexContent()

    def "Any without arguments should return true for non-empty list"() {
        given:
        TSimple q = generateQueryableWithSimpleContent()

        expect:
        q.any()
    }

    def "Any without arguments should return false for empty list"() {
        given:
        TSimple q = generateQueryableWithoutContent()

        expect:
        !q.any()
    }

    def "Any with given argument matching an element should return true"() {
        given:
        TSimple q = generateQueryableWithSimpleContent()

        expect:
        q.any(new Predicate<SimpleTestObject>() {
            @Override
            boolean test(SimpleTestObject simpleTestObject) {
                simpleTestObject.aString == "abc"
            }
        })
    }

    def "Any with given argument matching no element should return false"() {
        given:
        TSimple q = generateQueryableWithSimpleContent()

        expect:
        !q.any(new Predicate<SimpleTestObject>() {
            @Override
            boolean test(SimpleTestObject simpleTestObject) {
                simpleTestObject.aString == "____"
            }
        })
    }

    def "CastTo should cast to target type"() {
        given:
        TComplex q = generateQueryableWithComplexContent()

        when:
        Queryable<SimpleTestObject> cast = q.castTo(SimpleTestObject.class)

        then:
        cast instanceof TSimple

        and:
        cast.toList()[1].aString == "hello"
    }

    def "Count without argument should return number of elements"() {
        given:
        TSimple q = generateQueryableWithSimpleContent()

        expect:
        q.count() == 6
    }

    def "Count with given argument matching some elements should return number of matching elements"() {
        given:
        TSimple q = generateQueryableWithSimpleContent()

        expect:
        q.count(new Predicate<SimpleTestObject>() {
            @Override
            boolean test(SimpleTestObject simpleTestObject) {
                simpleTestObject.aInteger == 8
            }
        }) == 2
    }

    def "Count with given argument matching no elements should return zero"() {
        given:
        TSimple q = generateQueryableWithSimpleContent()

        expect:
        q.count(new Predicate<SimpleTestObject>() {
            @Override
            boolean test(SimpleTestObject simpleTestObject) {
                simpleTestObject.aString == "___"
            }
        }) == 0
    }

    def "Min should return minimum value of selected property of all elements"() {
        given:
        TSimple q = generateQueryableWithSimpleContent()

        expect:
        q.min(new Function<SimpleTestObject, Comparable>() {
            @Override
            Comparable apply(SimpleTestObject simpleTestObject) {
                simpleTestObject.aInteger
            }
        }) == -45
    }

    def "Min should throw IllegalStateException if there are no elements"() {
        given:
        TSimple q = generateQueryableWithoutContent()

        when:
        q.min(new Function<SimpleTestObject, Comparable>() {
            @Override
            Comparable apply(SimpleTestObject simpleTestObject) {
                simpleTestObject.aInteger
            }
        })

        then:
        thrown(IllegalStateException)
    }

    def "MinElement should return the element containing the minimum value of the selected property of all elements"() {
        given:
        TSimple q = generateQueryableWithSimpleContent()

        expect:
        q.minElement(new Function<SimpleTestObject, Comparable>() {
            @Override
            Comparable apply(SimpleTestObject simpleTestObject) {
                simpleTestObject.aInteger
            }
        }).aDouble == 1

    }

    def "MinElement should return null if there are no elements"() {
        given:
        TSimple q = generateQueryableWithoutContent()

        expect:
        q.minElement(new Function<SimpleTestObject, Comparable>() {
            @Override
            Comparable apply(SimpleTestObject simpleTestObject) {
                simpleTestObject.aInteger
            }
        }) == null
    }

    def "Max should return maximum value of selected property of all elements"() {
        given:
        TSimple q = generateQueryableWithSimpleContent()

        expect:
        q.max(new Function<SimpleTestObject, Comparable>() {
            @Override
            Comparable apply(SimpleTestObject simpleTestObject) {
                simpleTestObject.aDouble
            }
        }) == 9998.4656
    }

    def "Max should throw IllegalStateException if there are no elements"() {
        given:
        TSimple q = generateQueryableWithoutContent()

        when:
        q.max(new Function<SimpleTestObject, Comparable>() {
            @Override
            Comparable apply(SimpleTestObject simpleTestObject) {
                simpleTestObject.aDouble
            }
        })

        then:
        thrown(IllegalStateException)
    }

    def "MaxElement should return the element containing the maximum value of the selected property of all elements"() {
        given:
        TSimple q = generateQueryableWithSimpleContent()

        expect:
        q.maxElement(new Function<SimpleTestObject, Comparable>() {
            @Override
            Comparable apply(SimpleTestObject simpleTestObject) {
                simpleTestObject.aDouble
            }
        }).aInteger == 9

    }

    def "MaxElement should return null if there are no elements"() {
        given:
        TSimple q = generateQueryableWithoutContent()

        expect:
        q.maxElement(new Function<SimpleTestObject, Comparable>() {
            @Override
            Comparable apply(SimpleTestObject simpleTestObject) {
                simpleTestObject.aInteger
            }
        }) == null
    }

    def "Select should return Queryable of selected properties"() {
        given:
        TSimple q = generateQueryableWithSimpleContent()

        when:
        def result = q.select(new Function<SimpleTestObject, Object>() {
            @Override
            Object apply(SimpleTestObject simpleTestObject) {
                simpleTestObject.aInteger
            }
        })

        then:
        result.toList() == [8, 9, 10, 7, 8, -45]
    }

    def "Select should return Queryable of selected properties with operations executed on them"() {
        given:
        TSimple q = generateQueryableWithSimpleContent()

        when:
        def result = q.select(new Function<SimpleTestObject, Object>() {
            @Override
            Object apply(SimpleTestObject simpleTestObject) {
                simpleTestObject.aInteger * simpleTestObject.aInteger
            }
        })

        then:
        result.toList() == [64, 81, 100, 49, 64, 2025]
    }

    def "Select should return Queryable of maps containing selected properties"() {
        given:
        TSimple q = generateQueryableWithSimpleContent()

        when:
        def result = q.select(new Function<SimpleTestObject, Map>() {
            @Override
            Map apply(SimpleTestObject simpleTestObject) {
                [
                        aInteger: simpleTestObject.aInteger,
                        aDouble : simpleTestObject.aDouble
                ]
            }
        })

        then:
        result.toList().get(1)["aInteger"] == 9

        and:
        result.toList().get(1)["aDouble"] == 9998.4656
    }

    def "SelectMany should return Queryable of anonymous object containing selected properties"() {
        given:
        TSimple q = generateQueryableWithSimpleContent()
        TComplex subQ = generateQueryableWithComplexContent()

        when:
        def result = q.selectMany(new Function<SimpleTestObject, Queryable<ComplexTestObject>>() {
            @Override
            Queryable<ComplexTestObject> apply(SimpleTestObject simpleTestObject) {
                subQ.where(new Predicate<ComplexTestObject>() {
                    @Override
                    boolean test(ComplexTestObject complexTestObject) {
                        simpleTestObject.aInteger == complexTestObject.aInteger
                    }
                })
            }
        })

        then:
        result.toList().get(2).getString() == "hello"
    }

    def "Single should return single element if query returns only one element"() {
        given:
        TSimple q = generateQueryableWithSimpleContent()

        when:
        def result = q.where(new Predicate<SimpleTestObject>() {
            @Override
            boolean test(SimpleTestObject simpleTestObject) {
                simpleTestObject.aInteger == 7
            }
        }).single()

        then:
        result.aInteger == 7
    }

    def "Single should throw exception if query returns no element"() {
        given:
        TSimple q = generateQueryableWithoutContent()

        when:
        q.single()

        then:
        thrown(IllegalStateException)
    }

    def "Single should throw exception if query returns more than one element"() {
        given:
        TSimple q = generateQueryableWithSimpleContent()

        when:
        q.single()

        then:
        thrown(IllegalStateException)
    }

    def "SingleOrDefault should return single element if query returns only one element"() {
        given:
        TSimple q = generateQueryableWithSimpleContent()

        when:
        def result = q.where(new Predicate<SimpleTestObject>() {
            @Override
            boolean test(SimpleTestObject simpleTestObject) {
                simpleTestObject.aInteger == 7
            }
        }).singleOrDefault()

        then:
        result.aInteger == 7
    }

    def "SingleOrDefault should return default element if query returns no element"() {
        given:
        TSimple q = generateQueryableWithoutContent()

        expect:
        q.singleOrDefault() == null
    }

    def "SingleOrDefault should throw exception if query returns more than one element"() {
        given:
        TSimple q = generateQueryableWithSimpleContent()

        when:
        q.singleOrDefault()

        then:
        thrown(IllegalStateException)
    }

    def "ToList should convert queryable with elements to list"() {
        given:
        TSimple q = generateQueryableWithSimpleContent()

        when:
        def result = q.select(new Function<SimpleTestObject, Object>() {
            @Override
            Object apply(SimpleTestObject simpleTestObject) {
                simpleTestObject.aInteger
            }
        }).toList()

        then:
        result instanceof List

        and:
        result == [8, 9, 10, 7, 8, -45]
    }

    def "ToList should convert queryable without elements to list"() {
        given:
        TSimple q = generateQueryableWithoutContent()

        expect:
        q.toList() == []
    }

    def "Where should return only elements where condition is true"() {
        given:
        TSimple q = generateQueryableWithSimpleContent()

        when:
        def result = q.where(new Predicate<SimpleTestObject>() {
            @Override
            boolean test(SimpleTestObject simpleTestObject) {
                simpleTestObject.aInteger == 8
            }
        })

        then:
        result.count() == 2

        and:
        result.toList().get(0).aInteger == 8

        and:
        result.toList().get(1).aInteger == 8
    }

    def "Where should return no elements if condition is false for every element"() {
        given:
        TSimple q = generateQueryableWithSimpleContent()

        when:
        def result = q.where(new Predicate<SimpleTestObject>() {
            @Override
            boolean test(SimpleTestObject simpleTestObject) {
                simpleTestObject.aString == "___"
            }
        })

        then:
        result.count() == 0

        and:
        result.toList() == []
    }
}
