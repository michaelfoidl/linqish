package at.michaelfoidl.linqish.tests.comparator

import at.michaelfoidl.linqish.helper.Comparator
import at.michaelfoidl.linqish.tests.queryable.helpers.SimpleTestObject
import spock.lang.Specification


class ComparatorTests extends Specification {
    def "Compare should compare two instances of Integer"() {
        given:
        int a = 4
        int b = 5

        expect:
        Comparator.compare(a, b) < 0
    }

    def "Compare should compare two instances of Double"() {
        given:
        double a = 4.5
        double b = 1.2

        expect:
        Comparator.compare(a, b) > 0
    }

    def "Compare should compare two instances of Long"() {
        given:
        long a = 45687979
        long b = 45687979

        expect:
        Comparator.compare(a, b) == 0
    }

    def "Compare should compare two instances of Boolean"() {
        given:
        boolean a = false
        boolean b = true

        expect:
        Comparator.compare(a, b) < 0
    }

    def "Compare should compare two instances of Character"() {
        given:
        char a = 'a'
        char b = 'b'

        expect:
        Comparator.compare(a, b) < 0
    }

    def "Compare should compare two instances of String"() {
        given:
        String a = "m"
        String b = "c"

        expect:
        Comparator.compare(a, b) > 0
    }

    def "Compare should compare two instances of Byte"() {
        given:
        byte a = 45
        byte b = 45

        expect:
        Comparator.compare(a, b) == 0
    }

    def "Compare should compare two instances of Short"() {
        given:
        short a = 4
        short b = 5

        expect:
        Comparator.compare(a, b) < 0
    }

    def "Compare should compare two instances of Float"() {
        given:
        float a = 4.1
        float b = 4.0

        expect:
        Comparator.compare(a, b) > 0
    }

    def "Compare should throw exception when trying to compare instances of two different classes"() {
        given:
        float a = 4.1
        int b = 8

        when:
        Comparator.compare(a, b)

        then:
        thrown(IllegalStateException)
    }

    def "Compare should throw exception when trying to compare two complex types"() {
        given:
        SimpleTestObject a = new SimpleTestObject()
        SimpleTestObject b = new SimpleTestObject()

        when:
        Comparator.compare(a, b)

        then:
        thrown(IllegalStateException)
    }
}