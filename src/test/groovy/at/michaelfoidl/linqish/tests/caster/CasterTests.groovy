package at.michaelfoidl.linqish.tests.caster

import at.michaelfoidl.linqish.helper.Caster
import at.michaelfoidl.linqish.tests.queryable.helpers.ComplexTestObject
import at.michaelfoidl.linqish.tests.queryable.helpers.SimpleTestObject
import spock.lang.Specification


class CasterTests extends Specification {
    def "Cast should cast to an assignable instance"() {
        given:
        ComplexTestObject x = new ComplexTestObject()
        x.aString = "test"

        when:
        def result = Caster.cast(x, SimpleTestObject.class)

        then:
        result instanceof SimpleTestObject

        and:
        result.aString == "test"
    }

    def "Cast should throw exception when trying to cast to a not-assignable instance"() {
        given:
        int x = 4

        when:
        Caster.cast(x, SimpleTestObject.class)

        then:
        thrown(IllegalArgumentException)
    }
}